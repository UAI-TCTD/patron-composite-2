﻿Imports Anegamiento

Module Module1
    Dim fin As Boolean = False
    Dim root As ComponenteTerreno = New TerrenoAnegado(0)

    Sub Main()




        Dim resp As ConsoleKeyInfo

        Do
            Console.Write("Seleccione tipo de terreno  [(A)gua-(T)ierra-a(N)egado] ")
            resp = Console.ReadKey()
            Console.WriteLine(vbNewLine)
            If resp.KeyChar = "N" Then 'Si es anegado lo divido en 4

                root = New TerrenoAnegado(0)
                AnalizarTerreno(root)


            ElseIf resp.KeyChar = "T" Then
                root = New TerrenoTierra(0)
                fin = True
            ElseIf resp.KeyChar = "A" Then
                root = New TerrenoAgua(0)
                fin = True
            End If

        Loop Until resp.KeyChar = "A" Or resp.KeyChar = "T" Or resp.KeyChar = "N"




        Console.WriteLine("total porcentaje agua: " & root.PorcentajeAgua)
        Console.WriteLine("total porcentaje tierra: " & root.PorcentajeTierra)
        Console.ReadKey()
    End Sub


    Private Sub AnalizarTerreno(terreno As ComponenteTerreno)

        Dim resp As ConsoleKeyInfo


        If TypeOf terreno Is TerrenoAnegado Then
            'si esta anegado, lo divido en 4
            'para mejorar esto, se puder hacer polimorfico un método de division de terreno, 


            For x As Integer = 1 To 4

                Do

                    Console.Write("Seleccione tipo de terreno en cuadrante " + x.ToString + " [(A)gua-(T)ierra-a(N)egado] ")
                    resp = Console.ReadKey()
                    Console.WriteLine(vbNewLine)
                Loop Until resp.KeyChar = "A" Or resp.KeyChar = "T" Or resp.KeyChar = "N"

                If resp.KeyChar = "A" Then
                    terreno.AgregarHijo(New TerrenoAgua(x))
                ElseIf resp.KeyChar = "T" Then
                    terreno.AgregarHijo(New TerrenoTierra(x))
                ElseIf resp.KeyChar = "N" Then
                    terreno.AgregarHijo(New TerrenoAnegado(x))
                End If
            Next

            'analizo los hijos
            For Each hijo As ComponenteTerreno In terreno.ObtenerHijos
                AnalizarTerreno(hijo)
            Next




        End If




    End Sub




End Module

