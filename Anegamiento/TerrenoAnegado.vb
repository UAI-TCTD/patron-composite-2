﻿Public Class TerrenoAnegado
    Inherits ComponenteTerreno

    Public Sub New(cuadrante As Integer)
        MyBase.New(cuadrante)
    End Sub
    Private _hijos As New List(Of ComponenteTerreno)

    Public Overrides ReadOnly Property PorcentajeAgua As Double
        Get

            Dim tot As Double
            For Each t In _hijos
                tot += t.PorcentajeAgua
            Next
            Return tot / 4

        End Get
    End Property

    Public Overrides ReadOnly Property PorcentajeTierra As Double
        Get
            Dim tot As Double
            For Each t In _hijos
                tot += t.PorcentajeTierra
            Next
            Return tot / 4
        End Get
    End Property

    Public Overrides Function AgregarHijo(componente As ComponenteTerreno) As Object
        _hijos.Add(componente)
    End Function

    Public Overrides Function EliminarHijo(componente As ComponenteTerreno) As Object
        _hijos.Remove(componente)
    End Function

    Public Overrides Function ObtenerHijos() As List(Of ComponenteTerreno)
        Return _hijos
    End Function


End Class
