﻿Public MustInherit Class ComponenteTerreno

    Public ReadOnly Property Cuadrante As Integer
        Get
            Return _cuadrante
        End Get
    End Property
    Private _cuadrante As Integer
    Public Sub New(cuadrante As Integer)
        _cuadrante = cuadrante
    End Sub



    MustOverride ReadOnly Property PorcentajeAgua() As Double
    MustOverride ReadOnly Property PorcentajeTierra() As Double
    MustOverride Function AgregarHijo(componente As ComponenteTerreno)
    MustOverride Function EliminarHijo(componente As ComponenteTerreno)
    MustOverride Function ObtenerHijos() As List(Of ComponenteTerreno)
End Class
